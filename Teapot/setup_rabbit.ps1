param([String]$RabbitDllPath = "not specified")

$RabbitDllPath = Resolve-Path $RabbitDllPath
Write-Host "Rabbit DLL path:"
Write-Host $RabbitDllPath -ForegroundColor Green

Set-ExecutionPolicy Unrestricted

$absoluteRabbitDLLPath = Resolve-Path $RabbitDllPath

Write-Host "Absolute Rabbit DLL Path: "
Write-Host $absoluteRabbitDLLPath -ForegroundColor Green

[Reflection.Assembly]::LoadFile($absoluteRabbitDLLPath)

Write-Host "Setting up RabbitMQ Connection Factory" -ForegroundColor Green
$factory = New-Object RabbitMQ.Client.ConnectionFactory
$hostNameProp = [RabbitMQ.Client.ConnectionFactory].GetProperty("HostName")
$hostNameProp.SetValue($factory, "localhost")

$userNameProp = [RabbitMQ.Client.ConnectionFactory].GetProperty("UserName")
$userNameProp.SetValue($factory, "guest")

$passwordProp = [RabbitMQ.Client.ConnectionFactory].GetProperty("Password")
$passwordProp.SetValue($factory, "guest")

$createConnectionMethod = [RabbitMQ.Client.ConnectionFactory].GetMethod("CreateConnection", [Type]::EmptyTypes)

$connection = $createConnectionMethod.Invoke($factory, "instance,public", $null, $null, $null)

Write-Host "Setting up RabbitMQ model"
$model = $connection.CreateModel()

Write-Host "Creating Queue"
$model.QueueDeclare("OneWayQueue", $true, $false, $false, $null)

Write-Host "Complete"