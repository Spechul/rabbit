﻿using RabbitMQ.Client;
using System;
using System.Text;

namespace Teapot.Rabbit
{
    class RabbitSender : IDisposable
    {
        #region fields
        private readonly string HostName = Properties.Credentials.HostName;
        private readonly string UserName = Properties.Credentials.UserName;
        private readonly string Password = Properties.Credentials.Password;
        private readonly string QueueName = Properties.Credentials.QueueName;
        private readonly string ExchangeName = Properties.Credentials.ExchangeName;
        private readonly string IsDurable = Properties.Credentials.IsDurable;
        // may be needed
        private readonly string VirtualHost = "";
        private readonly int Port = 0;

        private ConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _model;
        #endregion

        #region ctors
        public RabbitSender()
        {
            SetupRabbitMQ();
        }
        #endregion

        #region private methods
        private void SetupRabbitMQ()
        {
            _connectionFactory = new ConnectionFactory()
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password,
            };

            if (!string.IsNullOrEmpty(VirtualHost))
                _connectionFactory.VirtualHost = VirtualHost;

            if (Port > 0)
                _connectionFactory.Port = Port;

            _connection = _connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
        }
        #endregion

        #region public methods
        public void Send(string message)
        {
            var properties = _model.CreateBasicProperties();
            properties.Persistent = true;

            byte[] messageBuffer = Encoding.Default.GetBytes(message);

            _model.BasicPublish(ExchangeName, QueueName, properties, messageBuffer);
        }
        #endregion

        #region interfaces
        public void Dispose()
        {
            _connection.Close();
            _model.Abort();
            _connectionFactory = null;
            GC.SuppressFinalize(this);
        }
        #endregion
    }
}
