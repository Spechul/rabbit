﻿using System;
using System.Globalization;
using System.Windows.Data;
using System.Windows.Media;

namespace Teapot.ValueConverters
{
    public class BoolToColorConverter : IValueConverter
    {
        public object Convert(object value, Type targetType, object parameter, CultureInfo culture)
        {
            bool val = (bool)value;

            if (val == true)
                return Colors.Green;
            else
                return Colors.Gray;
        }

        public object ConvertBack(object value, Type targetType, object parameter, CultureInfo culture)
        {
            if (value == Brushes.Green)
                return true;
            else
                return false;
        }
    }
}
