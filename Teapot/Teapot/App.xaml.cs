﻿using System.Windows;
using Teapot.ViewModels;

namespace Teapot
{
	/// <summary>
	/// Логика взаимодействия для App.xaml
	/// </summary>
	public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MainWindowViewModel vm = new MainWindowViewModel
            {
                TeapotViewModel = new TeapotViewModel(),
            };

            MainWindow main = new MainWindow();
            main.DataContext = vm;
            main.Closing += vm.OnWindowClosing;
            main.Show();
        }
    }
}
