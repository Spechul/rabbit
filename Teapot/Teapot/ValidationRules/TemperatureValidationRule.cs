﻿using System;
using System.Globalization;
using System.Windows.Controls;

namespace Teapot.ValidationRules
{
    class TemperatureValidationRule : ValidationRule
    {
        public override ValidationResult Validate(object value, CultureInfo cultureInfo)
        {
            int temperature;
            try
            {
                temperature = int.Parse((string)value);
                if (temperature > 100 || temperature < 0)
                    throw new ArgumentException("temperature must be within range (0, 100)");
            }
            // c# 6+
            catch (Exception e) when (e is ArgumentException || e is FormatException || e is OverflowException)
            {
                return new ValidationResult(false, string.Format("parsing cannot be performed with exception \"{0}\"", e.Message));
            }

            return ValidationResult.ValidResult;
        }
    }
}
