﻿using System;
using System.Threading;
using System.Threading.Tasks;
using System.Web.Script.Serialization;
using System.Windows.Input;
using Commons.Models;
using Teapot.Rabbit;
using Teapot.ViewModels.Commands;
using Commons.ViewModels;

namespace Teapot.ViewModels
{
    public class TeapotViewModel : LesserTeapotViewModel, IDisposable
    {
        #region fields
        private ICommand _startSendingInfo;
        private CancellationTokenSource _cancellationTokenSource;
        private CancellationToken _cancellationToken;
        private Task _sendTask = null;
        private RabbitSender _rabbitSender;
        #endregion


        #region properties
        #endregion

        #region commands
        public ICommand StartSendingInfo
        {
            get
            {
                if (_startSendingInfo == null)
                {
                    _startSendingInfo = new RelayCommand(
                        p => CanExecuteStartSendingInfo(),
                        p =>
                        {
                            if (Teapot.IsSending)
                                ExecuteStartSendingInfo(_cancellationToken);
                        });
                }
                return _startSendingInfo;
            }
        }
        #endregion

        #region ctors
        public TeapotViewModel()
        {
            Teapot = new TeaPot();
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
        }
        #endregion

        #region interfaces
        public void Dispose()
        {
            try
            {
                _rabbitSender.Dispose();
            }
            // can get when already deleted
            catch (Exception e) { }

            _cancellationTokenSource.Cancel();
        }
        #endregion

        #region private methods
        private bool CanExecuteStartSendingInfo()
        {
            return true;
        }

        private void ExecuteStartSendingInfo(CancellationToken ct)
        {
            if (Teapot == null)
                throw new NullReferenceException("Teapot in the model was null");
            if (_sendTask == null)
            {
                _sendTask = new Task(new Action(() => Send(Teapot)), ct);
                _sendTask.Start();
            }
        }

        private void Send(TeaPot teaPot)
        {
            while (true)
            {
                if (_cancellationToken.IsCancellationRequested)
                    return;

                if (!teaPot.IsSending || !teaPot.IsTurnedOn)
                {
                    Thread.Sleep(100);
                    continue;
                }

                using (_rabbitSender = new RabbitSender())
                {
                    if (teaPot.IsSending && teaPot.IsTurnedOn)
                    {
                        // set unix timestamp
                        teaPot.TimeStamp = (Int32)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;

                        string message = new JavaScriptSerializer().Serialize(teaPot);

                        _rabbitSender.Send(message);
                    }
                }
                Thread.Sleep(2000);
            }
        }
        #endregion
    }
}
