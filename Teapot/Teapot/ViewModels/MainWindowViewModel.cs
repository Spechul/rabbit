﻿using System;
using System.ComponentModel;


namespace Teapot.ViewModels
{
    public class MainWindowViewModel : IDisposable
    {
        private TeapotViewModel _teapotViewModel;
        public TeapotViewModel TeapotViewModel
        {
            get => _teapotViewModel;
            set
            {
                _teapotViewModel?.Dispose();
                _teapotViewModel = value;
            }
        }

        public void Dispose()
        {
            TeapotViewModel.Dispose();
        }

        public void OnWindowClosing(object sender, CancelEventArgs args)
        {
            Dispose();
        }
    }
}