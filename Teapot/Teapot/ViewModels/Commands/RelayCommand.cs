﻿using System;
using System.Windows.Input;

namespace Teapot.ViewModels.Commands
{
    public class RelayCommand : ICommand
    {
        #region fields
        private Predicate<object> _canExecute;
        private Action<object> _execute;
        #endregion

        #region ctors
        public RelayCommand(Predicate<object> canExecute, Action<object> execute)
        {
            this._canExecute = canExecute;
            this._execute = execute;
        }
        #endregion

        #region events
        public event EventHandler CanExecuteChanged
        {
            add { CommandManager.RequerySuggested += value; }
            remove { CommandManager.RequerySuggested -= value; }
        }
        #endregion

        #region interfaces
        public bool CanExecute(object parameter)
        {
            return _canExecute(parameter);
        }

        public void Execute(object parameter)
        {
            _execute(parameter);
        }
        #endregion
    }
}
