﻿using Monitor.ViewModels;
using System.Windows;

namespace Monitor
{
    /// <summary>
    /// Логика взаимодействия для App.xaml
    /// </summary>
    public partial class App : Application
    {
        private void Application_Startup(object sender, StartupEventArgs e)
        {
            MainWindowViewModel vm = new MainWindowViewModel();
            MainWindow main = new MainWindow(vm);
            main.Show();
        }
    }
}
