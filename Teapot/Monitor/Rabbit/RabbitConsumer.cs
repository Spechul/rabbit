﻿using RabbitMQ.Client;
using RabbitMQ.Client.Events;
using System;
using System.IO;
using System.Text;
using Commons.Models;

namespace Monitor.Rabbit
{
    class RabbitConsumer : IDisposable
    {
        #region fields
        private readonly string HostName = Properties.Credentials.HostName;
        private readonly string UserName = Properties.Credentials.UserName;
        private readonly string Password = Properties.Credentials.Password;
        private readonly string QueueName = Properties.Credentials.QueueName;
        private readonly string ExchangeName = Properties.Credentials.ExchangeName;
        private readonly string IsDurable = Properties.Credentials.IsDurable;
        // may be needed
        private readonly string VirtualHost = "";
        private int Port = 0;

        private ConnectionFactory _connectionFactory;
        private IConnection _connection;
        private IModel _model;
        #endregion

        #region delegates
        public delegate void OnReceiveMessage(string message);
        #endregion

        #region properties
        public bool Enabled { get; set; }
        #endregion

        #region ctors
        public RabbitConsumer()
        {
            _connectionFactory = new ConnectionFactory()
            {
                HostName = HostName,
                UserName = UserName,
                Password = Password,
                RequestedHeartbeat = 10,
            };

            if (!string.IsNullOrEmpty(VirtualHost))
                _connectionFactory.VirtualHost = VirtualHost;

            if (Port > 0)
                _connectionFactory.Port = Port;

            _connection = _connectionFactory.CreateConnection();
            _model = _connection.CreateModel();
            _model.BasicQos(0, 1, false);
        }
        #endregion

        #region public methods
        public TeaPot ListenQueue()
        {
            var consumer = new QueueingBasicConsumer(_model);
            string message = "";
            try
            {
                _model.BasicConsume(QueueName, false, consumer);

                BasicDeliverEventArgs deliveryArgs = consumer.Queue.Dequeue();
                _model.BasicAck(deliveryArgs.DeliveryTag, false);
                message = Encoding.Default.GetString(deliveryArgs.Body);
            }
            // when window closes it may be raised
            catch (EndOfStreamException e) { }


            return TeaPot.FromJSON(message);
        }
        #endregion

        #region interfaces
        public void Dispose()
        {
            try
            {
                _model.Abort();
                _connection.Close();
                _connectionFactory = null;
            }
            // bad, but will fix
            catch (Exception e)
            { }
        }
        #endregion
    }
}
