﻿using Monitor.ViewModels;
using System.Windows;

namespace Monitor
{
    /// <summary>
    /// Логика взаимодействия для MainWindow.xaml
    /// </summary>
    public partial class MainWindow : Window
    {
        public MainWindow(MainWindowViewModel vm)
        {
            DataContext = vm;
            Closing += vm.OnWindowClosing;
            InitializeComponent();
        }
    }
}
