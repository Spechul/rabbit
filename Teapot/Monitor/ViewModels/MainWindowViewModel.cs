﻿using System;
using System.ComponentModel;

namespace Monitor.ViewModels
{
    public class MainWindowViewModel : IDisposable
    {
        #region fields
        private TeapotCollectionViewModel _teapotCollectionViewModel;
        #endregion

        #region properties
        public TeapotCollectionViewModel TeapotCollectionViewModel
        {
            get => _teapotCollectionViewModel;
            set
            {
                _teapotCollectionViewModel?.Dispose();
                _teapotCollectionViewModel = value;         
            }
        }
        #endregion

        #region ctors
        public MainWindowViewModel()
        {
            TeapotCollectionViewModel = new TeapotCollectionViewModel();
        }
        #endregion

        #region interfaces
        public void Dispose()
        {
            _teapotCollectionViewModel.Dispose();
        }
        #endregion

        #region public methods
        public void OnWindowClosing(object sender, CancelEventArgs args)
        {
            Dispose();
        }
        #endregion
    }
}
