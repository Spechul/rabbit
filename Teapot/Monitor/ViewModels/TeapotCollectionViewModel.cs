﻿using Monitor.Rabbit;
using System;
using System.Linq;
using System.Threading;
using System.Threading.Tasks;
using Commons.Models;
using Monitor.Collections;
using Commons.ViewModels;

namespace Monitor.ViewModels
{
    public class TeapotCollectionViewModel : ViewModelBase, IDisposable
    {
        #region const fields

        private const int Timeout = 10;

        #endregion

        #region fields
        private CancellationTokenSource _cancellationTokenSource;
        private CancellationToken _cancellationToken;
        private object _teapotsLock = new object();

        private ThreadSafeObservableCollection<LesserTeapotViewModel> _teaPots;
        private LesserTeapotViewModel _selectedTeaPot;
        private int _selectedIndex;

        // it will be surely disposed on Window Close event
        private RabbitConsumer _rabbitConsumer;
        #endregion

        #region properties
        public ThreadSafeObservableCollection<LesserTeapotViewModel> TeaPots
        {
            get => _teaPots;
            set
            {
                _teaPots = value;
                OnPropertyChanged(nameof(TeaPots));
            }
        }

        public LesserTeapotViewModel SelectedTeaPot
        {
            get => _selectedTeaPot;
            set
            {
                _selectedTeaPot = value;
                OnPropertyChanged(nameof(SelectedTeaPot));
            }
        }
        #endregion

        #region ctors
        public TeapotCollectionViewModel()
        {
            //TeaPots = new ThreadSafeObservableCollection<TeaPot>();
            TeaPots = new ThreadSafeObservableCollection<LesserTeapotViewModel>();
            _cancellationTokenSource = new CancellationTokenSource();
            _cancellationToken = _cancellationTokenSource.Token;
            //_dispatcher = Dispatcher.CurrentDispatcher;
            SelectedTeaPot = new LesserTeapotViewModel();
            //SelectedTeaPot = new TeaPot();
            //TeaPots.CollectionChanged += TeaPots_CollectionChanged;

            Task.Factory.StartNew(() => ListenQueue(), _cancellationToken);

            Task.Factory.StartNew(() => DeleteObsoleteTeaPot(), _cancellationToken);

            /* 
             * this pause is needed to setup both tasks correctly. other way one won't be cancelled 
             * if the app is closed immediately after starting or in other situation like this
             */
            Thread.Sleep(250);
        }

        // a try to reselect item
        /*private void TeaPots_CollectionChanged(object sender, System.Collections.Specialized.NotifyCollectionChangedEventArgs e)
        {
            if(_selectedIndex < TeaPots.Count)
                SelectedTeaPot = TeaPots[_selectedIndex];
        }*/
        #endregion

        #region private methods
        private void DeleteObsoleteTeaPot()
        {
            while (true)
            {
                Thread.Sleep(100);
                int currentTime = (Int32)DateTime.UtcNow.Subtract(new DateTime(1970, 1, 1)).TotalSeconds;
                foreach (var teapot in TeaPots)
                {
                    if (Math.Abs(teapot.TimeStamp - currentTime) > Timeout)
                    {
                        lock (_teapotsLock)
                        {
                            TeaPots.Remove(teapot);
                            break;
                        }
                    }
                }
            }
        }

        private void ListenQueue()
        {
            while (true)
            {
                if (_cancellationToken.IsCancellationRequested)
                    return;

                TeaPot gottenTeaPot = null;
                using (_rabbitConsumer = new RabbitConsumer())
                {
                    gottenTeaPot = _rabbitConsumer.ListenQueue();
                }

                if (gottenTeaPot == null)
                {
                    Thread.Sleep(100);
                }
                else
                {
                    int length = TeaPots.Count;
                    bool teaPotExists = false;
                    for (int i = 0; i < length; i++)
                    {
                        if (TeaPots[i].Id == gottenTeaPot.Id)
                        {
                            lock (_teapotsLock)
                            {
                                TeaPots[i].UpdateProperties(gottenTeaPot);
                                teaPotExists = true;
                            }
                        }
                    }
                    if (!teaPotExists)
                    {
                        lock (_teapotsLock)
                        {
                            TeaPots.Add(new LesserTeapotViewModel (gottenTeaPot));
                        }
                    }
                }
            }
        }
        #endregion

        #region public methods
        public void Dispose()
        {
            _cancellationTokenSource.Cancel();
            _rabbitConsumer.Dispose();
        }
        #endregion
    }
}