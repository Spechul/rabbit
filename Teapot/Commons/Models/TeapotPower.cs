﻿namespace Commons.Models
{
    public enum TeapotPower
    {
        Low = 0,
        Medium,
        High,
    }
}
