﻿using System.Diagnostics;
using System.Web.Script.Serialization;

namespace Commons.Models
{
    public class TeaPot
    {
        //  when adding new property don't forget to move it also to UpdateProperties method
        #region properties
        public int Id { get; set; }
        public int Temperature { get; set; }
        public TeapotPower Power { get; set; }
        public bool IsTurnedOn { get; set; }
        public bool IsSending { get; set; }
        public bool IsLightTurnedOn { get; set; }
        // to know when it was sent
        public int TimeStamp { get; set; }
        #endregion

        #region ctors
        public TeaPot()
        {
            Id = Process.GetCurrentProcess().Id;
        }
        #endregion

        #region static methods
        public static TeaPot FromJSON(string json)
        {
            JavaScriptSerializer serializer = new JavaScriptSerializer();
            return serializer.Deserialize<TeaPot>(json);
        }
        #endregion

        #region UpdateProperties method
        public void UpdateProperties(TeaPot other)
        {
            Id = other.Id;
            Temperature = other.Temperature;
            Power = other.Power;
            IsTurnedOn = other.IsTurnedOn;
            IsSending = other.IsSending;
            IsLightTurnedOn = other.IsLightTurnedOn;
            TimeStamp = other.TimeStamp;
        }
        #endregion

        #region public methods
        public override string ToString()
        {
            return Id.ToString();
        }
        #endregion
    }
}
