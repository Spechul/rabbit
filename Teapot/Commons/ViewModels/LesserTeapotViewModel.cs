﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Commons.Models;

namespace Commons.ViewModels
{
    public class LesserTeapotViewModel : ViewModelBase
    {
        #region properties
        public TeaPot Teapot { get; set; }

        public int Id
        {
            get => Teapot.Id;
            set
            {
                Teapot.Id = value;
                OnPropertyChanged(nameof(Id));
            }
        }

        public int Temperature
        {
            get => Teapot.Temperature;
            set
            {
                Teapot.Temperature = value;
                OnPropertyChanged(nameof(Temperature));
            }
        }

        public TeapotPower Power
        {
            get => Teapot.Power;
            set
            {
                Teapot.Power = value;
                OnPropertyChanged(nameof(Power));
            }
        }

        public bool IsTurnedOn
        {
            get => Teapot.IsTurnedOn;
            set
            {
                Teapot.IsTurnedOn = value;
                OnPropertyChanged(nameof(IsTurnedOn));
            }
        }

        public bool IsLightTurnedOn
        {
            get => Teapot.IsLightTurnedOn;
            set
            {
                Teapot.IsLightTurnedOn = value;
                OnPropertyChanged(nameof(IsLightTurnedOn));
            }
        }

        public bool IsSending
        {
            get => Teapot.IsSending;
            set
            {
                Teapot.IsSending = value;
                OnPropertyChanged(nameof(IsSending));
            }
        }

        public int TimeStamp
        {
            get => Teapot.TimeStamp;
            set
            {
                Teapot.TimeStamp = value;
                OnPropertyChanged(nameof(TimeStamp));
            }
        }

        public void UpdateProperties(TeaPot gotten)
        {
            Teapot.UpdateProperties(gotten);
            OnPropertyChanged(nameof(Id));
            OnPropertyChanged(nameof(Temperature));
            OnPropertyChanged(nameof(Power));
            OnPropertyChanged(nameof(IsLightTurnedOn));
            OnPropertyChanged(nameof(IsLightTurnedOn));
            OnPropertyChanged(nameof(IsSending));
            OnPropertyChanged(nameof(TimeStamp));
        }
        #endregion

        #region ctors
        public LesserTeapotViewModel()
        {
            Teapot = new TeaPot();
        }

        public LesserTeapotViewModel(TeaPot teaPot)
        {
            Teapot = teaPot;
        }
        #endregion

        #region overriden members
        public override string ToString()
        {
            return Teapot.ToString();
        }
        #endregion
    }
}
